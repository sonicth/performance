#include <benchmark/benchmark.h>
#include <vector>

using namespace std;
namespace gb = benchmark;

static void bench_create(gb::State &state)
{
	while (state.KeepRunning())
	{
		vector<int> v;
		//(void) v;
		v.reserve(1);
	}

}
BENCHMARK(bench_create);

static void bench_push(gb::State &state)
{
	while (state.KeepRunning())
	{
		vector<int> v;
		v.reserve(1);
		v.push_back(43);
	}

}
BENCHMARK(bench_push);

BENCHMARK_MAIN();