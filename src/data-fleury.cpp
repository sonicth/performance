//from C++ talk, probably a games developer, if I remember correctly from ubisoft montreal 

#include <iostream>
#include <string>
#include <chrono>
#include <algorithm>

struct Data
{
    Data() 
    {
        for (int i = 0; i < 64; ++i)
            values[i] = i;
    }
    int values[64];
};

struct MyClass
{
	// int64_t m_Total = 0;
	volatile int64_t m_Total = 0;
	template <bool ISLOCAL>
	void UpdateTotal(int *values, int count);
};

template <>
void MyClass::UpdateTotal<false>(int *values, int count)
{
	for (int i = 0; i < count; ++i)
		m_Total += values[i];
}
template <>
void MyClass::UpdateTotal<true>(int *values, int count)
{
	int64_t total = 0;
	for (int i = 0; i < count; ++i)
		total += values[i];
	m_Total = total;
}

namespace ch = std::chrono;

class Timer
{
    decltype(ch::high_resolution_clock::now()) start;
    std::string name;
public:
    Timer(std::string name_ = "Timer")
        : start{ ch::high_resolution_clock::now() }
        , name{ name_ }
    {}
    ~Timer()
    {
        auto stop = ch::high_resolution_clock::now();
        auto duration = ch::duration_cast<ch::milliseconds>(stop - start);
        std::cerr << "**" << name << " duration: "<<duration.count()<<" milliseconds" <<std::endl;
    }
};


using namespace std;


constexpr auto OBJECTCOUNT = 1 << 20;
constexpr auto LOOPCOUNT = 1<<1;
template <typename T>
void dataTestOuter(T dataar)
{
    volatile int total = 0;
    int i, j;
    for (j = 0; j < OBJECTCOUNT; ++j)
        for (i = 0; i < 64; ++i)
            total += dataar[j].values[i];
}

template <typename T>
void dataTestInner(T dataar)
{
    volatile int total = 0;
    int i, j;
    for (i = 0; i < 64; ++i)
        for (j = 0; j < OBJECTCOUNT; ++j)
			total += dataar[j].values[i];
}	

void dataTest()
{
    Data *dataar = new Data[OBJECTCOUNT]; // huge size

    {
    Timer t("Data Timing (outer)");
    for (int i = 0; i < LOOPCOUNT; ++i)
        dataTestOuter(dataar);
    }
    
    {
    Timer t("Data Timing (inner)");
    for (int i = 0; i < LOOPCOUNT; ++i)
        dataTestInner(dataar);
    }

    delete[] dataar;
}

void localVarTest()
{
	constexpr auto VALUECOUNT = 1 << 25;
	constexpr auto LOOPCOUNT2 = 10;
    auto *iar = new int[VALUECOUNT]; // huge size
	MyClass myobj;

	generate(iar, iar + VALUECOUNT, []{ return rand(); });
	{
    	Timer t("Member variable test");
		for (int i = 0; i < LOOPCOUNT2; ++i)
			myobj.UpdateTotal<false>(iar, VALUECOUNT);
    }
    
	generate(iar, iar + VALUECOUNT, []{ return rand(); });
    {
    	Timer t("Local variable test");
		for (int i = 0; i < LOOPCOUNT2; ++i)
			myobj.UpdateTotal<true>(iar, VALUECOUNT);

    }

}


int main()
{
    dataTest();
	// localVarTest();
}