#include <iostream>

#include <unordered_map>
#include <map>

#include <absl/container/flat_hash_map.h>
#include <absl/container/node_hash_map.h>

#define _SILENCE_STDEXT_HASH_DEPRECATION_WARNINGS
#include <sparsehash/sparse_hash_map>
#include <sparsehash/dense_hash_map>

class Logger
{
public:
	Logger()
	{
		std::cerr << "##**log** ";
	}
	~Logger()
	{
		std::cerr << "##" << std::endl;
	}

	template <typename T>
	friend Logger &operator<< (Logger &l, T const &val)
	{
		std::cerr << val;
		return l;
	}
};

namespace mb
{
	template <typename Key, typename T>
	class HashDummy
	{
		T _val{};
	public:
		T &operator[] (Key const &idx)
		{
			//Logger{} << "key " << idx;
			return _val;
		}

		bool contains(Key const &v)
		{
			return false;
		}
	};


	template <typename Key, typename T>
	class HashStdUnorderedMap
	{
		std::unordered_map<Key, T> _internal;
	public:
		T &operator[] (Key const &idx)
		{
			//Logger{} << "key " << idx;
			return _internal[idx];
		}

		bool contains(Key const &v)
		{
			return _internal.find(v) != _internal.end();
		}
	};

	template <typename Key, typename T>
	class HashStdMap
	{
		std::map<Key, T> _internal;
	public:
		T &operator[] (Key const &idx)
		{
			//Logger{} << "key " << idx;
			return _internal[idx];
		}
		bool contains(Key const &v)
		{
			return _internal.find(v) != _internal.end();
		}
	};

	template <typename Key, typename T>
	class AbslFlat
	{
		absl::flat_hash_map<Key, T> _internal;
	public:
		T &operator[] (Key const &idx)
		{
			//Logger{} << "key " << idx;
			return _internal[idx];
		}
		bool contains(Key const &v)
		{
			return _internal.find(v) != _internal.end();
		}
	};

	template <typename Key, typename T>
	class AbslNode
	{
		absl::node_hash_map<Key, T> _internal;
	public:
		T &operator[] (Key const &idx)
		{
			//Logger{} << "key " << idx;
			return _internal[idx];
		}
		bool contains(Key const &v)
		{
			return _internal.find(v) != _internal.end();
		}
	};

	template <typename T>
	T emptyKey();


	template <> int emptyKey<int>() { return -1; }
	template <> std::string emptyKey<std::string>() { return std::string{""}; }

	template <typename Key, typename T>
	class GoogleDense
	{
		google::dense_hash_map<Key, T> _internal;
	public:
		GoogleDense()
		{
			_internal.set_empty_key(emptyKey<Key>());
		}

		T &operator[] (Key const &idx)
		{
			//Logger{} << "key " << idx;
			return _internal[idx];
		}
		bool contains(Key const &v)
		{
			return _internal.find(v) != _internal.end();
		}
	};

	template <typename Key, typename T>
	class GoogleSparse
	{
		google::sparse_hash_map<Key, T> _internal;
	public:
		T &operator[] (Key const &idx)
		{
			//Logger{} << "key " << idx;
			return _internal[idx];
		}
		bool contains(Key const &v)
		{
			return _internal.find(v) != _internal.end();
		}
	};
	
}

#include <vector>
#include <list>
#include <algorithm>
namespace mb
{
	template <typename T>
	size_t hash(T const &key);


	template <>
	size_t hash<int>(int const &key)
	{
		return static_cast<size_t>(key);
	}

	template <>
	size_t hash<std::string>(std::string const &key)
	{
		size_t h = 0;
		for (auto &c : key)
			h = (h << 1) ^ size_t(c);

		return static_cast<size_t>(h);
	}

	template <typename Key, typename T>
	class Hash1
	{
		static size_t constexpr HASH_SIZE = 100;
		using Item_t = std::pair<Key, T>;
		using Bucket_t = std::list<Item_t>;
		using Hash_t = std::vector<Bucket_t>;


		Hash_t _internal = Hash_t(HASH_SIZE);
	public:

		class Inserter
		{
			Bucket_t &_bucket;
			Key _key;
			Inserter(Key const &key, Bucket_t &bucket)
				: _bucket(bucket)
				, _key{ key }
			{}

		public:
			friend class Hash1;

			Item_t &operator= (T const &value)
			{
				if (_bucket.empty())
					// empty bucket...
				{
					// insert empty value and return
					return *_bucket.insert(begin(_bucket), { _key, value });
				}
				else if (auto it = find(begin(_bucket), end(_bucket), Item_t{ _key, value }); it != end(_bucket))
					// non-empty bucket AND there exists a hashed value already
				{
					*it = { _key, value };
					return *it;
				}
				else 
					// non-empty bucket, but no value found
				{
					return *_bucket.insert(it, { _key, value });
				}
			}
		};

		size_t hashIdx(Key const &key)
		{
			int idx = hash(key) % HASH_SIZE;
			return idx;
		}

		Inserter operator[] (Key const &key)
		{
			return Inserter(key, _internal[hashIdx(key)]);
		}

		bool contains(Key const &key)
		{
			auto &bucket = _internal[hashIdx(key)];
			return std::find_if(begin(bucket), end(bucket), [&key](auto &item) { return item.first == key; }) != end(bucket);
		}
	};
}