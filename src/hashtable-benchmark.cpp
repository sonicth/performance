#include "hashtable.h"

#include <benchmark/benchmark.h>
#include <iostream>	

using namespace std;

//template <typename T>
//void fillWithRandomNumbers(T &set)
//{
//	constexpr NUM_NUMBERS = 10000;
//	for (int i = 0; i < NUM_NUMBERS / 10; ++i)
//	{
//		int r = rand();
//		for (int j = 0; j < 10; ++j)
//		{
//			set.insert(r);
//			r = r * 2 - 1;
//		}
//	}
//}


//
//struct sortedVectorSet
//{
//	std::vector<unsigned> data;
//
//	template <class Engine>
//	void generate(Engine &e, std::size_t size)
//	{
//		data.resize(size);
//		std::generate(begin(data), end(data), e);
//		std::sort(begin(data), end(data));
//
//	}
//
//	bool contains(unsigned i) const
//	{
//		return std::binary_search(begin(data), end(data), i);
//	}
//};


template<template<class, class> class HashTable>
static void BM_HashtableInsertInts(benchmark::State& state)
{
	constexpr size_t SETSIZE = 100000;
	constexpr size_t HMAX = 0xffff;
	
	while (state.KeepRunning())
	{
		HashTable<int, int> h;
		auto f = [i = 0]() mutable { return (rand() + i++); };
		
		for (int i = 0; i < SETSIZE; i += 3)
		{
			h[f() & HMAX] = i;
		}

		for (int i = 0; i < SETSIZE * 100; i += 3)
		{
			bool found = h.contains(i & HMAX);
		}

	}
}

template <template<class, class> class HashTable, typename Key, typename Value>
struct Cover
{
	using key = Key;
	using value = Value;
	HashTable<Key, Value> data;

	template <class Engine>
	void generate(Engine& e, std::size_t size)
	{
		for (auto i = 0u; i != size; ++i)
		{
			data.insert({ e(), i });
		}
	}

	bool contains(Key k) const
	{
		return data.find(k) != data.end();
	}
};

using CoverStdUMapStrI = Cover<std::unordered_map, std::string, int>;

template<typename HashFront>
static void BM_HashtableInsertStdString(benchmark::State &state)
{
	constexpr size_t SETSIZE = 100000;

	while (state.KeepRunning())
	{
		HashFront h;
		auto f = [i = 0]() mutable { return std::to_string(rand() + i++); };
		h.generate(f, SETSIZE);

		for (int i = 0; i < SETSIZE; i += 3)
		{
			h.contains(std::to_string(i));
		}

	}
}




BENCHMARK(BM_HashtableInsertInts<mb::HashDummy>)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_HashtableInsertInts<mb::HashStdUnorderedMap>)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_HashtableInsertInts<mb::HashStdMap>)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_HashtableInsertInts<mb::Hash1>)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_HashtableInsertInts<mb::AbslFlat>)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_HashtableInsertInts<mb::AbslNode>)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_HashtableInsertInts<mb::GoogleSparse>)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_HashtableInsertInts<mb::GoogleDense>)->Unit(benchmark::kMillisecond);

//BENCHMARK(BM_HashtableInsertStdString<mb::HashDummy>)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_HashtableInsertStdString<CoverStdUMapStrI>)->Unit(benchmark::kMillisecond);
//BENCHMARK(BM_HashtableInsertStdString<mb::HashStdMap>)->Unit(benchmark::kMillisecond);
//BENCHMARK(BM_HashtableInsertStdString<mb::Hash1>)->Unit(benchmark::kMillisecond);
//BENCHMARK(BM_HashtableInsertStdString<absl::flat_hash_map>)->Unit(benchmark::kMillisecond);
//BENCHMARK(BM_HashtableInsertStdString<absl::node_hash_map>)->Unit(benchmark::kMillisecond);


// Run the benchmark
BENCHMARK_MAIN();
