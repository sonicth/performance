void SomeFunction()
{
	long long x = 0;
	long long maxval = 1000 * 1000;
	for (long long j = 0; j < maxval; ++j)
	{
		++x;
	}
}

#include <benchmark/benchmark.h>
#include <iostream>
#include <vector>
#include <array>
#include <list>
#include <set>
#include <unordered_set>
using namespace std;

template <typename T>
void fillWithRandomNumbers(T &set)
{
	constexpr NUM_NUMBERS = 10000;
	for (int i = 0; i < NUM_NUMBERS / 10; ++i)
	{
		int r = rand();
		for (int j = 0; j < 10; ++j)
		{
			set.insert(r);
			r = r * 2 - 1;
		}
	}
}

struct stdSet
{
	std::set<unsigned> data;

	template <class Engine>
	void generate(Engine &e, std::size_t size)
	{
		for (auto i = 0u; i != size; ++i)
		{
			data.insert(e());
		}
	}

	bool contains(unsigned i) const
	{
		return data.find(i) != data.end();
	}
};


struct stdUoSet
{
	std::unordered_set<unsigned> data;

	template <class Engine>
	void generate(Engine &e, std::size_t size)
	{
		for (auto i = 0u; i != size; ++i)
		{
			data.insert(e());
		}
	}

	bool contains(unsigned i) const
	{
		return data.find(i) != data.end();
	}
};


struct linearVectorSet
{
	std::vector<unsigned> data;

	template <class Engine>
	void generate(Engine &e, std::size_t size)
	{
		data.resize(size);
		std::generate(begin(data), end(data), e);

	}

	bool contains(unsigned i) const
	{
		//for (auto &el : data)
		//{
		//	if (el == i)
		//		return true;
		//}
		//return false;
		return std::find(begin(data), end(data), i) != end(data);

	}
};


struct sortedVectorSet
{
	std::vector<unsigned> data;

	template <class Engine>
	void generate(Engine &e, std::size_t size)
	{
		data.resize(size);
		std::generate(begin(data), end(data), e);
		std::sort(begin(data), end(data));

	}

	bool contains(unsigned i) const
	{
		return std::binary_search(begin(data), end(data), i);
	}
};

template <typename T>
static void BM_CacheFriendlyStdContainer(benchmark::State& state) {

  while (state.KeepRunning())
  {
	  for (long long i = 0; i < 1000LL; ++i)
	  {
		  T v;
		  for (int j = 0; j < i; ++j)
		  {
			  v.push_back(j);
		  }

	  }
  }

}

template<typename T>
static void BM_CacheFriendlyStdSet(benchmark::State& state)
{
	while (state.KeepRunning())
	{
		for (long long i = 0; i < 1000LL; ++i)
		{
			T s;
			for (int j = 0; j < i; ++j)
			{
				s.insert(j);
			}

		}

	}
}


template<typename T>
static void BM_CacheFriendly(benchmark::State& state)
{
	constexpr size_t SETSIZE = 1000;
	while (state.KeepRunning())
	{
		T s;
		auto f = [i = 0]() mutable { return rand() + i++; };
		s.generate(f, SETSIZE);

		for (int i = 0; i < SETSIZE; i += 3)
		{
			s.contains(i);
		}
	}
}


//// Register the function as a benchmark
//BENCHMARK(BM_CacheFriendlyStdContainer<std::vector<int>>)->Unit(benchmark::kMillisecond);
////BENCHMARK(BM_CacheFriendlyStdContainer<std::array<int, 1000>>)->Unit(benchmark::kMillisecond);
//BENCHMARK(BM_CacheFriendlyStdContainer<std::list<int>>)->Unit(benchmark::kMillisecond);
//BENCHMARK(BM_CacheFriendlyStdSet<std::set<int>>)->Unit(benchmark::kMillisecond);
//BENCHMARK(BM_CacheFriendlyStdSet<std::unordered_set<int>>)->Unit(benchmark::kMillisecond);
//BENCHMARK(BM_CacheFriendlyStdSet<std::unordered_multiset<int>>)->Unit(benchmark::kMillisecond);

BENCHMARK(BM_CacheFriendly<stdSet>)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_CacheFriendly<stdUoSet>)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_CacheFriendly<linearVectorSet>)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_CacheFriendly<sortedVectorSet>)->Unit(benchmark::kMillisecond);

// Run the benchmark
BENCHMARK_MAIN();
