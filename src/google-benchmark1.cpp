void SomeFunction()
{
	long long x = 0;
		for (int j = 0; j < 1000*100; ++j)
		{
			++x;
		}
}

#include <benchmark/benchmark.h>

static void BM_SomeFunction(benchmark::State& state) {
  // Perform setup here
  //for (auto _ : state)
  while (state.KeepRunning())
  {
    // This code gets timed
    SomeFunction();
  }
}
// Register the function as a benchmark
BENCHMARK(BM_SomeFunction);
// Run the benchmark
BENCHMARK_MAIN();
